<div style="font-size:180%">Taller 1</div>
<div style="font-size:160%">Electiva Desarrollo Web Avanzado - 2018-1</div>
<div style="font-size:120%">Docente: Pablo Bejarano - Duración 3 horas</div>

Entrega: Jueves 1 de Marzo de 2018 hasta las 9:30 pm.

El desarrollo de esta actividad se entregará mediante medios digitales en un repositorio de Bitbucket privado al cual adicionarán al docente como participante, sólo será calificado el último commit realizado antes de la hora máxima de entrega.

<div style="font-size:110%; font-weight:bold; border-bottom:1px solid gray;">Automatización (50%):</div>

1. Escriba un script en SHELL llamado **dev** y registrelo como un comando que le permita ahorrar tiempo en procesos muy comúnes en desarrollo. El script funcionará como un CLI de las herramientas comúnes de desarrollo, como por ejemplo **Ionic CLI**, y sus funciones para éste ejercicio académico se basarán en funciones comúnes de control de versiones descritas a continuación:
		
	**a.** Usualmente el desarrollador realiza un push después de crear un commit, y para ello adiciona al seguimiento todos los archivos usando ``git add -A``, para despues ejecutar ``git commit -m "{texto del commit}"`` y por fin poder realizar un push usando ``git push -u {origen} {rama (que usualmente es la master)}``. desarrolle la funcionalidad ``saveit`` que permita automátizar dicho proceso recibiendo como parámetros: 
	
	1. el flag del tipo de git addd {-A, -u, -e, ...}
	2. el texto asociado al commit
	3. el origen al que irá el push
	4. la rama
		
	Un ejemplo del comando se vería así: ``dev saveit -A "un gran commit" origin master``	 
		
	**b.** Otro caso muy común es el de inicializar un proyecto, pues además de correr el comando ``git init``, también se suele definir un servidor remoto para que no tengamos que escribir toda la URL para cada push ``git remote add {nombre} {URL}`` y el archivo ``.gitignore``que define los elementos a ignorar en el seguimiento del proyecto. Usted creará el comando ``genesis``, que hará todo lo anterior de una manera muy sencilla y transparente para el desarrollador. El comando se ejecutará de la siguiente manera:
	
	1. Inicializa el repositorio con ``git init``
	2. Consulta al desarrollador por la opción de definir un remoto: ``Do you want to add a remote? [y | n]``
	3. Al seleccionar ``y`` solicita al desarrollador el nombre del remoto ``Type the remote name:``
	4. Solicita al desarrollador la URL: ``Type the {nombre del remoto} location:``
	5. Pregunta nuevamente el paso 2 y repite hasta que el desarrollador termine de adicionar remotos.
	6. Pregunta al desarrollador si desea adicionar un archivo .gitignore ``Do you want to add a .gitignore file? [y | n]``
	7. Si el desarrollador marca ``y`` entonces el CLI pregunta si desea usar un template ``Do you want to use a .gitignore template? [y | n]
	8. Si el usuario selecciona ``y``entonces mostrará los posibles templates a usar: ``Choose the templates to add by the code separated by commas: [1] Compiled Source, [2] Packages, [3] Logs and Databases, [4] OS generated Files`` el usuario entonces podrá seleccionar los que quiera.
	9. Por el contrario si el usuario no quiere adicionar un template, el CLI preguntará si desea escribir su propia versión: ``Do you wan to write your own version? [y | n]`` de ser así el CLI abrirá nano para que el desarrollador escriba su propio .gitignore (El nano abre editando ya el archivo .gitignore en la ubicación actual de la terminal).
	10. Al final el CLI notificará que todo el setup se terminó correctamente .

	Templates:
	
	```
# Compiled source #
###################
*.com
*.class
*.dll
*.exe
*.o
*.so
	```
	
	```
	# Packages #
############
# it's better to unpack these files and commit the raw source
# git has its own built in compression methods
*.7z
*.dmg
*.gz
*.iso
*.jar
*.rar
*.tar
*.zip
```

	```
	# Logs and databases #
	######################
	*.log
	*.sql
	*.sqlite
	```

	```
	# OS generated files #
	######################
	.DS_Store
	.DS_Store?
	._*
	.Spotlight-V100
	.Trashes
	ehthumbs.db
	Thumbs.db
	```
	
<div style="font-size:110%; font-weight:bold; border-bottom:1px solid gray;">Patrones de diseño (50%):</div>

Para este ejercicio usted tendrá que modificar los archivos base para
obtener los resultados esperados segun las ejecuciones del **index.php** y verificará si existen inconsistencias con los requeriminetos (y las reparará de ser así). Identifique los patrones de diseño que mejor se aplican para resolver este ejercicio y desarrollelos.

1. **Entidades:**
    * Todos los aliens tienen los siguientes atributos: `nombre, edad, especie,
    planeta de origen y moral`, la cual por defecto es **"neutral"**.

    * Todos los aliens usan como medio de comunicación la telepatía, por eso usted
    deberá definir una constante de clase `COMUNICACION = "telepaticamente"`

    * Todos los aliens tienen un método llamado interact, el cual varía según el
    tipo de alien, los aliens buenos retornan `[COMUNICACION] dice: Hola 
    terricola mi nombre es [NOMBRE], vinimos en son de paz`, mientras los aliens
    malos retornan `[COMUNICACION] dice: Hola terricola, rindanse ante la 
    invasión de [NOMBRE]`

    * Todos los aliens tiene un método whoIAm que retorna: `[COMICACION] dice: 
    Mi nombre es [NOMBRE], vengo del plantea [PLANETA], soy un [ESPECIE] y soy [MORAL]`

    * Los aliens **buenos** vienen de Marte, Luna y Venus. Su moral es "bueno"
    * Los aliens **malos** vienen de Plutón, Jupiter y Saturno. Su moral es "malo"

    * Los aliens de **Jupiter** son los unicos que hablan con acento (telepaticamente) 
    así que todas las "s" las pronuncian como "sh"

    * Los aliens de **Venus** gimen (telepaticamente) despues de cada ","
    así que todas las "," se reemplazan por ", hmmm,"

    * Todos los aliens de un planeta específico no requiren de un planeta en su
    constructor, **ej:** los MarsAlien tienen que ser todos de marte.

    * **Los aliens buenos** deben tener los siguientes métodos:
     - `salvarPlaneta(Planeta planeta)`, cambia el estado del planeta
     - `llamarACasa()`, retorna "Llamando a Caaaaasaaaaa"

    * **Los aliens malos** deben tener los siguientes métodos:
     - `destruirPlaneta(Planeta planeta)`, cambia el estado del planeta
     - `pedirRefuerzos()`, retorna "Solicitando refuerzos Muahahaha"

    * Todos los atributos de los aliens deben estar encapsulados
    * Se require una fabrica abstracta de aliens AlienFactory
    
    * El planeta tiene un nombre y estado "a salvo" o "destruido"; siempre
    un planeta está "a salvo" a menos que lo destruyan.
    * El planeta tiene un método status que retorna su estado
    * Todos los atributos de los planetas deben estar ecapsulados.